package com.lanke.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Contest {
  // Some liberties were taken to keep attributes public to avoid getters and
  // setters so this could be hacked quickly
  
  long contestId;
  final int salaryCap = 50000;
  final HashMap<Validate.Position, Integer> rosterPlayerCount = new HashMap<>();
  private List<FantasyGame> games = new ArrayList<>();

  public Contest(long id, Validate.ContestType contestType) {
    this.contestId = id;
    setUpContestroster(contestType);
  }

  /*
  Fantasy roster can be setup depending on sports contest.
   */
  public void setUpContestroster(Validate.ContestType contestType) {
    if (contestType == Validate.ContestType.NFL) {
      this.rosterPlayerCount.put(Validate.Position.QB, 1);
      this.rosterPlayerCount.put(Validate.Position.RB, 2);
      this.rosterPlayerCount.put(Validate.Position.WR, 3);
      this.rosterPlayerCount.put(Validate.Position.TE, 1);
      this.rosterPlayerCount.put(Validate.Position.FLEX, 1);
      this.rosterPlayerCount.put(Validate.Position.DST, 1);
    }
  }

  public void addGame(FantasyGame game) {
    this.games.add(game);
  }

  public List<FantasyGame> getGames() {
    return this.games;
  }
}
