package com.lanke.test;

import java.util.*;

public class Validate {
  // Some liberties were taken to keep attributes public to avoid getters and
  // setters so this could be hacked quickly

  HashSet<LineupItem> lineup;
  enum Position {QB, RB, WR, TE, DST, FLEX, Dummy};
  enum ContestType {NFL, NBA, MLS};

  public static void main(String[] args) throws Exception {
    boolean validateAllrules = false;

    System.out.println("\nStart\n");

    Player p1 = new Player(1l,111,"p1", 7200, Validate.Position.QB);
    Player p2 = new Player(2l,222,"p2", 5600, Validate.Position.RB);
    Player p3 = new Player(3l,333,"p3", 5100, Validate.Position.RB);
    Player p4 = new Player(4l,111,"p4", 6100, Validate.Position.WR);
    Player p5 = new Player(5l,222,"p5", 5800, Validate.Position.WR);
    Player p6 = new Player(6l,444,"p6", 5400, Validate.Position.WR);
    Player p7 = new Player(7l,333,"p7", 4500, Validate.Position.TE);
    Player p8 = new Player(8l,222,"p8", 6400, Validate.Position.WR);
    Player p9 = new Player(9l,111,"p9", 3100, Validate.Position.DST);
    //Player p10 = new Player(10l, 444, "p9", 3100, Validate.Position.DST);

    //Used for local debugging.
    //System.out.println("\nPlayer\n");

    LineupItem lineUp = new LineupItem();
    lineUp.addToLineup(new TeamPlayer(p1, Position.QB));
    lineUp.addToLineup(new TeamPlayer(p2, Position.RB));
    lineUp.addToLineup(new TeamPlayer(p3, Position.RB));
    lineUp.addToLineup(new TeamPlayer(p4, Position.WR));
    lineUp.addToLineup(new TeamPlayer(p5, Position.WR));
    lineUp.addToLineup(new TeamPlayer(p6, Position.WR));
    lineUp.addToLineup(new TeamPlayer(p7, Position.FLEX));
    lineUp.addToLineup(new TeamPlayer(p8, Position.TE));
    lineUp.addToLineup(new TeamPlayer(p9, Position.DST));
    //lineUp.addToLineup(new TeamPlayer(p10, Position.QB));

    //Used for local debugging.
    //System.out.println("\nLineItem\n");

    Contest contest = new Contest(001,ContestType.NFL);
    FantasyGame game1 = new FantasyGame(001L, 111,222);
    FantasyGame game2 = new FantasyGame(002L, 333,444);
    FantasyGame game3 = new FantasyGame(003L, 222,444);
    contest.addGame(game1);
    contest.addGame(game2);
    contest.addGame(game3);

    //Used for local debugging.
    //System.out.println("\nContest\n");

    //Validate all the rules
    Validate objValidate = new Validate();

    /* RULES
      1.The sum of player salary cannot exceed the contests max salary cap
      2.Any single player can only be used once
      3.The lineup cannot contain more than the required amount of players
      4.There can not be more than 3 players of a single team per game
      5.All roster positions listed in the contest must be filled by the lineup
      6.The lineup must encompass at least two games
     */

     // Some liberties were taken to keep attributes public to avoid getters and
     // setters so this could be hacked quickly
    //This method will check all the rules and fail early throwing an exception with error message
    validateAllrules = objValidate.validateRules(lineUp.getLineup(), contest);

    if (validateAllrules) {
      System.out.println("\n Grand Success \n");
    }
  }

  public boolean validateRules(List<TeamPlayer> lineup, Contest contest) throws Exception {
    int lineupSal = 0;
    int rosterPlayerCount = 0;
    List<Long> playerIds = new ArrayList<>();
    List<Integer> teamIds = new ArrayList<>();
    List<Validate.Position> linupPositions = new ArrayList<>();
    HashMap<Validate.Position, Integer> lineupRoster = new HashMap<>();

    for (TeamPlayer teamPlayer : lineup) {
      if (lineupSal > contest.salaryCap) {
        //Rule 1
        throw new Exception("Lineup salary exceeded");
      }
      else {
        lineupSal += teamPlayer.player.salary;
      }

      if (playerIds.contains(teamPlayer.player.playerId)) {
        //Rule 2
        throw new Exception("Player already in the lineup " + teamPlayer.player.playerId);
      }
      else {
        playerIds.add(teamPlayer.player.playerId);
      }

      teamIds.add(teamPlayer.player.teamId);
      if (Collections.frequency(teamIds, teamPlayer.player.teamId) > 3) {
        //Rule 4
        throw new Exception("Lineup cannot have more than 3 players from same team " + teamPlayer.player.teamId);
      }

      linupPositions.add(teamPlayer.fantasyPosition);
      if (lineupRoster.containsKey(teamPlayer.fantasyPosition)) {
        lineupRoster.put(teamPlayer.fantasyPosition, lineupRoster.get(teamPlayer.fantasyPosition) + 1);
      }
      else {
        lineupRoster.put(teamPlayer.fantasyPosition, 1);
      }
    }

    for (Map.Entry<Position, Integer> entry : contest.rosterPlayerCount.entrySet()) {

      if (!lineupRoster.containsKey(entry.getKey())) {
        // Rule 5
        throw new Exception("Required roster position missing from lineup ");
      }
      if (lineupRoster.get(entry.getKey()) != entry.getValue()) {
        // Rule 3
        throw new Exception("The lineup cannot contain more/less than the required amount of players ");
      }
      rosterPlayerCount += entry.getValue();
    }

    if (lineup.size() != rosterPlayerCount) {
      // Rule 3
      throw new Exception("The lineup cannot contain more/less than the required amount of players ");
    }

    // Assumption was made that 3 unique teamids are required as
    // Game 1: team 1 vs team 2
    // Game 2: team 2 vs team 3
    // Else we can compare team ids with the ids from the Games List in the contest class
    Set<Integer> uniqueTeamIds = new HashSet<>(teamIds);
    if (uniqueTeamIds.size() < 3) {
      // Rule 6
      throw new Exception("Lineup doesn't encompass 2 games ");
    }

    return true;
  }
}
