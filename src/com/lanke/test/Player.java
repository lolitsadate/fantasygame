package com.lanke.test;

import java.util.HashSet;
import java.util.Random;

public class Player {
  // Some liberties were taken to keep attributes public to avoid getters and
  // setters so this could be hacked quickly
  
  long playerId;
  int teamId;
  String name;
  int salary;
  Validate.Position position;
  static HashSet<Long> usedPlayerId = new HashSet<Long>();

  public Player(long playerId, int teamId, String name, int salary, Validate.Position position ) {
    this.playerId = playerId;
    this.teamId = teamId;
    this.name = name;
    this.salary = salary;
    this.position = position;
  }

}
