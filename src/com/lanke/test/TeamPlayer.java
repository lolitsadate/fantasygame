package com.lanke.test;

public class TeamPlayer {
  // Some liberties were taken to keep attributes public to avoid getters and
  // setters so this could be hacked quickly
  
  Player player;
  Validate.Position fantasyPosition;

  public  TeamPlayer(Player player, Validate.Position position) {
    this.player = player;
    this.fantasyPosition = position;
  }
}
