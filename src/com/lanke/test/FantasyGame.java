package com.lanke.test;

public class FantasyGame {
  // Some liberties were taken to keep attributes public to avoid getters and
  // setters so this could be hacked quickly
  long gameId;
  int homeTeamId;
  int awayTeamId;

  public FantasyGame(long gameId, int homeTeamId, int awayTeamId) {
    this. gameId = gameId;
    this.homeTeamId = homeTeamId;
    this.awayTeamId = awayTeamId;
  }
}
