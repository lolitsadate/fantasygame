package com.lanke.test;

import java.util.ArrayList;
import java.util.List;

public class LineupItem {
  private List<TeamPlayer> teamLineup = new ArrayList<TeamPlayer>();

  public void addToLineup(TeamPlayer player) {
    this.teamLineup.add(player);
  }

  public List<TeamPlayer> getLineup() {
    return teamLineup;
  }
}